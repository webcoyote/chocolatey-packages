

$install = $(Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install")

$file32 = $(Get-ChildItem -r $install | Where-Object { $_.Name -match '^sed-[0-9\.]+\.exe$' }).FullName
$file64 = $(Get-ChildItem -r $install | Where-Object { $_.Name -match '^sed-[0-9\.]+-x64\.exe$' }).FullName
$target = Join-Path $install "sed.exe"

$file = $file32
if (Get-ProcessorBits 64) {
$forceX86 = $env:chocolateyForceX86
if ($forceX86) {
  Write-Debug "User specified '-x86' so forcing 32-bit"
} else {
  if ($file64bit -ne $null -and $file64bit -ne '') {
    $file = $file64
  }
}
}

Copy-Item $file $target
Remove-Item $file32
Remove-Item $file64
