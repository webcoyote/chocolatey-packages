
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
New-Item -ItemType Directory -Force -Path $root | Out-Null
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'zip'
	url           = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-4.0.0-beta1/swigwin-4.0.0-beta1.zip/download'
	url64bit      = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-4.0.0-beta1/swigwin-4.0.0-beta1.zip/download'
	checksum      = 'fc28b79246df226b5abe692688f500066a237aa41a92ab91249aa7011f27cfbd'
	checksumType  = 'sha256'
	checksum64    = 'fc28b79246df226b5abe692688f500066a237aa41a92ab91249aa7011f27cfbd'
	checksumType64= 'sha256'

}
Install-ChocolateyZipPackage @packageArgs

