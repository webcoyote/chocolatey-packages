
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
New-Item -ItemType Directory -Force -Path $root | Out-Null
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'zip'
	url           = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-4.0.0/swigwin-4.0.0.zip/download'
	url64bit      = 'https://sourceforge.net/projects/swig/files/swigwin/swigwin-4.0.0/swigwin-4.0.0.zip/download'
	checksum      = '1391aecad92e365b960eb1a1db3866ca1beee61b3395c182298edbf323d1695a'
	checksumType  = 'sha256'
	checksum64    = '1391aecad92e365b960eb1a1db3866ca1beee61b3395c182298edbf323d1695a'
	checksumType64= 'sha256'

}
Install-ChocolateyZipPackage @packageArgs

