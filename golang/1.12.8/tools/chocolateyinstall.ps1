
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.8.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.8.windows-amd64.msi'
	checksum      = '73007ebd07ab3ef37efa21c93ce7c741030dfacaf1477dc05e656a0bedac5793'
	checksumType  = 'sha256'
	checksum64    = '9839254990dedc5647bf38611f7c1a7afc65f7fd6aaff977e05b17d4ec21fea6'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

