
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.12.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.12.windows-amd64.msi'
	checksum      = '850c0640689d03c79500a6f9f4ba52789f2df7b64413a2bbb4aa6bae391ed722'
	checksumType  = 'sha256'
	checksum64    = '6e648c27236ebd3741b6f09778423cdea73cab55e8c35cd04137d495f2bb2c47'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

