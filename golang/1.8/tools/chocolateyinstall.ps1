
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.8.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.8.windows-amd64.msi'
	checksum      = 'ab95f48fc86e6e3c4a5cd7851d66414c3e6b551df4bc905a9e5dcde802ad7377'
	checksumType  = 'sha256'
	checksum64    = 'b2e20d9d7cd7ce95812ba6762df641cc1d3a71c218469f155a0bf9fb0ba9d994'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

