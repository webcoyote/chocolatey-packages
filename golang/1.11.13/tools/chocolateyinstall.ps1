
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.13.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.13.windows-amd64.msi'
	checksum      = '59a281b5f6b857c0da9be22b48dcb2b80d66d12f059f690f248c246639d1f881'
	checksumType  = 'sha256'
	checksum64    = 'af89bbbdb6dcbbd35b08415cb25725fc9f785f514d5c40a9e342841344b38acd'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

