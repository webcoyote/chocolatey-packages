
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.11.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.11.windows-amd64.msi'
	checksum      = '68f5527c8e3a306b5a88d3135016df2198e23262df1a80886a1573b2aae9a634'
	checksumType  = 'sha256'
	checksum64    = 'ddf544beca0f218af58b81858d049f78dada256ab5c6431711edd4f4a9522420'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

