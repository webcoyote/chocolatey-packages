
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.11.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.11.windows-amd64.msi'
	checksum      = '6b4f64e7c1e221a189e4ee69189e1591436d6ea326d047d89e3e8319625033c0'
	checksumType  = 'sha256'
	checksum64    = 'f8ef6bd15a2657833c5f520490265d1431299856c8fce4a64390347d6c536e2e'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

