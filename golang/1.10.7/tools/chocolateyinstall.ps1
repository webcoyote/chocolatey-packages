
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.10.7.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.10.7.windows-amd64.msi'
	checksum      = 'b7b1810f98793cf90307426d14e3ef8a290b3699ec11f4358f4c7e5f85633029'
	checksumType  = 'sha256'
	checksum64    = 'd91b80be5d7a6dd6169d577e35984e99113fa2577ce91a939148454a29d9794b'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

