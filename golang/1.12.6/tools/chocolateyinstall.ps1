
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.6.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.6.windows-amd64.msi'
	checksum      = 'c95b46844c96d417f8414856cef61d2247df2806a1e8e1baf88a2f337e2ac4d2'
	checksumType  = 'sha256'
	checksum64    = 'a91ab71be78a610bf444a47e4194cf5fe07c58d06bbaedf1edad93b8a8513068'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

