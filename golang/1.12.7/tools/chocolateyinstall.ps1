
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'msi'
	url           = 'https://dl.google.com/go/go1.12.7.windows-386.msi'
	url64bit      = 'https://dl.google.com/go/go1.12.7.windows-amd64.msi'
	checksum      = 'b22e8dc56cf67b613d3b7f85baa75a911892950c36af152dbea9a173fb34cb82'
	checksumType  = 'sha256'
	checksum64    = 'dfd346a2ab0a0d3cc42ee8295b9026dccb3fc8f1b6578eedd3e79b6528e1aa39'
	checksumType64= 'sha256'

	silentArgs    = '/qn /norestart'
	validExitCodes= @(0, 3010, 1641)

}

New-Item -ItemType Directory -Force -Path $root | Out-Null
Install-ChocolateyPackage @packageArgs

