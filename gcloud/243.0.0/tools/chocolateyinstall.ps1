if (Get-Command "gcloud" -errorAction SilentlyContinue) {
    $env:CLOUDSDK_PYTHON = (gcloud components copy-bundled-python | Out-String).trim()
    gcloud components update --quiet

} catch {

	$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
	$packageArgs = @{
		packageName   = $env:ChocolateyPackageName
		unzipLocation = $root
		fileType      = 'exe'
		url           = 'https://dl.google.com/dl/cloudsdk/channels/rapid/GoogleCloudSDKInstaller.exe'
		url64bit      = 'https://dl.google.com/dl/cloudsdk/channels/rapid/GoogleCloudSDKInstaller.exe'
		checksum      = '987b4d4ddccdd169055d7547d089b09c52c39df10fb752cf626360480c5f3c51'
		checksumType  = 'sha256'
		checksum64    = '987b4d4ddccdd169055d7547d089b09c52c39df10fb752cf626360480c5f3c51'
		checksumType64= 'sha256'

		silentArgs    = '/S /allusers'
		validExitCodes= @(0, 3010, 1641)

	}

	New-Item -ItemType Directory -Force -Path $root | Out-Null
	Install-ChocolateyPackage @packageArgs

}

