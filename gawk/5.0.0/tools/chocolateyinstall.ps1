
$root = Join-Path "$(Split-Path -Parent $MyInvocation.MyCommand.Definition)" "install"
New-Item -ItemType Directory -Force -Path $root | Out-Null
$packageArgs = @{
	packageName   = $env:ChocolateyPackageName
	unzipLocation = $root
	fileType      = 'zip'
	url           = 'https://sourceforge.net/projects/ezwinports/files/gawk-5.0.0-w32-bin.zip/download'
	url64bit      = 'https://sourceforge.net/projects/ezwinports/files/gawk-5.0.0-w32-bin.zip/download'
	checksum      = 'f2176bf26260e8fb100b5446b49a8a102c8bda22cd7b3d394f2f74b813357d67'
	checksumType  = 'sha256'
	checksum64    = 'f2176bf26260e8fb100b5446b49a8a102c8bda22cd7b3d394f2f74b813357d67'
	checksumType64= 'sha256'

}
Install-ChocolateyZipPackage @packageArgs

